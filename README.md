# Phil's Light Box

Phil Whitworth needed a device to light up petri dishes for photographing cell colony growth.
The project is related to the Washington State University Veterinary Medicine program, though his needs mostly centered around using vision processing to identify colonies using vision processing.

The current devices for this work are very expensive, so having a less costly alternative would be very nice. So, we decided to build one. We chose to do it during the Washington State University Fall 2019 Hardware Hackathon to keep focused and it worked out: 
http://hardwarehackathon.eecs.wsu.edu/

The box has an enclosed chamber (made from a bucket turned upside down) with a camera fixed in the top, pointing down.
Around the camera is a ring of 24 Adafruit NeoPixel RGBW leds.
Below the dish is a platform with a light diffuser on top of a 8x8 matrix of Adafruit NeoPixel RGBW leds to provide backlighting.

The control panel on the front has a suite of controls for the device.

1. Main power on/off
2. Two light on/off switches to control the upper and lower light arrays
3. Four 10k slider potentiometers to control the RGBW components

Additionally, the interface has a 128x64 OLED screen. The screen shows you the current RGBW values and which sets of the lights are on in the enclosure.

### Hardware

Components included:

1. Arduino Uno Rev3: https://store.arduino.cc/usa/arduino-uno-rev3
2. Arduino Uno protoshield: http://bit.ly/36tS2Xs
3. OLED 128x64 SS1306, Yellow/White: http://bit.ly/2WCp430
4. Potentiometers, 10k, 45mm slider package: https://www.adafruit.com/product/4272
5. Single pull single throw rocker switches: http://bit.ly/2r9sf6K
6. One 220V round rocker switch: http://bit.ly/2WCoeDz
7. One Adafruit NeoPixel NeoMatrix - 64 RGBW - Cool White - ~6000K: https://www.adafruit.com/product/2872
8. One Adafruit NeoPixel Ring - 24 x 5050 RGBW LEDs w/ Integrated Drivers - Cool White - ~6000K: https://www.adafruit.com/product/2863
9. JST headers XH2.54 2p 3p 4p 5 pin 2.54mm Pitch Terminal Kit: http://bit.ly/34snaow
10. Plenty of wire and patience


### License

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license:
http://creativecommons.org/licenses/by-nc-sa/4.0/
