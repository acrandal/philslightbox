/*
  AnalogReadSerial

  Reads an analog input on pin 0, prints the result to the Serial Monitor.
  Graphical representation is available using Serial Plotter (Tools > Serial Plotter menu).
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/AnalogReadSerial
*/


#include <Adafruit_NeoPixel.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

// OLED configuration parameters
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
#define LINE1          0
#define LINE2         16
#define LINE3         32
#define LINE4         48
#define COLUMN1        0
#define COLUMN2       64

// Sliders for color controls
#define RED_POT_PIN A3
#define GREEN_POT_PIN A2
#define BLUE_POT_PIN A1
#define WHITE_POT_PIN A0

// Switches to turn sets of lights on/off
#define LOWER_LIGHTS_SWITCH_PIN 9
#define UPPER_LIGHTS_SWITCH_PIN 10

// NeoPixel light array settings
#define NEOPIXEL_PIN     12
#define NEOPIXEL_COUNT   88
#define LOWER_LIGHTS_COUNT 64
#define UPPER_LIGHTS_COUNT 24

// Setup the neopixel strips
Adafruit_NeoPixel neopixel_strip(NEOPIXEL_COUNT, NEOPIXEL_PIN, NEO_GRBW + NEO_KHZ800);

// OLED screen configuration
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// ************************************************************************************* //
void setup() {
  //Serial.begin(115200);

  pinMode(LOWER_LIGHTS_SWITCH_PIN, INPUT_PULLUP);
  pinMode(UPPER_LIGHTS_SWITCH_PIN, INPUT_PULLUP);

  // Initialize NeoPixel strips
  neopixel_strip.begin();
  neopixel_strip.show();

  // Initialize OLED screen
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  // Address 0x3C for 128x64 with the Chinese knockoffs
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { 
    //Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever - TODO: blink an error light
  }
  display.display();                // Initialize display with splash screen
  display.clearDisplay();           // Wipe screen
  
  // Configure OLED for tex modes
  display.setTextSize(2);           // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  
  display.setCursor(0, 0);          // Start at top-left corner
  display.cp437(true);              // Use full 256 char 'Code Page 437' font
  update_screen(0, 0, 0, 0, 1, 1);  // Init screen to defaults
}

// ***************************************************************** //
void loop() {
  // Read in current slider/pot values
  int redValue    = 255 - 255 * ((float) (10*(analogRead(RED_POT_PIN)/10)) / 1024);
  int greenValue  = 255 - 255 * ((float) (10*(analogRead(GREEN_POT_PIN)/10)) / 1024);
  int blueValue   = 255 - 255 * ((float) (10*(analogRead(BLUE_POT_PIN)/10)) / 1024);
  int whiteValue  = 255 - 255 * ((float) (10*(analogRead(WHITE_POT_PIN)/10)) / 1024);

  // Read in control switch values
  int lower_lights_switch_state = digitalRead(LOWER_LIGHTS_SWITCH_PIN);
  int upper_lights_switch_state = digitalRead(UPPER_LIGHTS_SWITCH_PIN);

  if(lower_lights_switch_state == LOW) {   // Switch is on (pulled low)
    for( int i = 0; i < LOWER_LIGHTS_COUNT; i++ ){
      neopixel_strip.setPixelColor(i, neopixel_strip.Color(redValue, greenValue, blueValue, whiteValue));
    }
  } else {
    for( int i = 0; i < LOWER_LIGHTS_COUNT; i++ ){
      neopixel_strip.setPixelColor(i, neopixel_strip.Color(0,0,0,0));
    }
  }

  if(upper_lights_switch_state == LOW) {   // Switch is on (pulled low)
    for( int i = LOWER_LIGHTS_COUNT; i < LOWER_LIGHTS_COUNT + UPPER_LIGHTS_COUNT; i++ ){
      neopixel_strip.setPixelColor(i, neopixel_strip.Color(redValue, greenValue, blueValue, whiteValue));
    }
  } else {
    for( int i = LOWER_LIGHTS_COUNT; i < LOWER_LIGHTS_COUNT + UPPER_LIGHTS_COUNT; i++ ){
      neopixel_strip.setPixelColor(i, neopixel_strip.Color(0,0,0,0));
    }
  }
  
  neopixel_strip.show();

  update_screen(redValue, greenValue, blueValue, whiteValue, lower_lights_switch_state, upper_lights_switch_state);
  
  delay(1);        // stability delay
}


// Re-Draw the OLED screen with all various data provided
void update_screen(int red, int green, int blue, int white, int lower_switch_status, int upper_switch_status) {
  
  display.clearDisplay();   // Wipe screen
  display.setCursor(COLUMN1, LINE1);     // Start at top-left corner
  display.println("Camera Box");

  display.setCursor(COLUMN1, LINE2);
  display.print("R:");
  display.print(red);

  display.setCursor(COLUMN1, LINE3);
  display.print("G:");
  display.print(green);
  
  display.setCursor(COLUMN1, LINE4);
  display.print("B:");
  display.print(blue);

  display.setCursor(COLUMN2, LINE2);
  display.print("W:");
  display.print(white);

  display.setCursor(COLUMN2, LINE3);
  display.print("LO:");
  if(lower_switch_status == LOW) {
    display.print("ON");
  } else {
    display.print("OFF");
  }
  
  display.setCursor(COLUMN2, LINE4);
  display.print("HI:");  
  if(upper_switch_status == LOW) {
    display.print("ON");
  } else {
    display.print("OFF");
  }
  
  display.display();
}
